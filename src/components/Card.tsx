import React, { useState } from 'react';

interface CardProps {
  title: string | number;
  children?: React.ReactNode
  icon?: string;
  onIconClick?: () => void
}

export const Card = React.memo((props: CardProps) => {
  const { title, icon, onIconClick, children } = props;
  const [isOpen, setIsOpen] = useState<boolean>(false);

  return (
    <div className="card mt-3" >
      <div className="card-header"
           onClick={() => setIsOpen(!isOpen)}>
        {title}
        <div className="pull-right">
          { icon ? <i className={icon} onClick={onIconClick} /> : null}
        </div>
      </div>

      {
        (children && isOpen) && <div className="card-body" >{children}</div>
      }
    </div>
  )
});
