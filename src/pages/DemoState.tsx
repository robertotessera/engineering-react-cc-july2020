import React, { useEffect, useState } from 'react';
import { Card } from '../components/Card';
import Axios from 'axios';
import { Series } from '../model/series';
import { NavBar } from '../components/Nabbar';

function DemoState() {
  let [count, setCount] = useState<number>(10);
  let [title, setTitle] = useState<string>('ciao ciao');
  let [result, setResult] = useState<Series[]>([])

  useEffect(() => {
    console.log('init')
    Axios.get<Series[]>('http://api.tvmaze.com/search/shows?q=soprano')
      .then(res => {
        setResult(res.data)
      })
  }, []);


  const inc = () => {
    setCount(++count)
  };

  function dec() {
    setCount(--count);
  };

  function getCount() {
    return 'il counter è' + count;
  }

  function gotoWebsite(url: string) {
    window.open(url)
  }

  return (
    <div className="center">
      {result.length} Risultati

      {
        result.map((item: Series, index: number) => {
          return (
            <Card
              key={item.show.id}
              title={(index+1) + ': ' + item.show.name}
              icon="fa fa-link"
              onIconClick={() => gotoWebsite(item.show.url)}
            >
              {item.show.summary}
            </Card>)
        })
      }

      <hr/>
      <Card title={result.length + ' results'} icon="fa fa-plus" onIconClick={inc} />
      <Card title={count} icon="fa fa-minus" onIconClick={dec} />

      <button onClick={inc}>+</button>
      <button onClick={dec}>-</button>
      <button onClick={() => setTitle('hello' + Math.random())}>Random title</button>
      <h1>{getCount()} {title}</h1>

      {/*<UIKit />*/}

    </div>
  )
}

export default DemoState;
