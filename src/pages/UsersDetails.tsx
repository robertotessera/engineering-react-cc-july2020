import React, { useEffect, useState } from 'react';
import Axios from 'axios';
import { Card } from '../components/Card';
import { RouteComponentProps } from 'react-router-dom';
import { User } from '../model/user';


export function UsersDetails(props: RouteComponentProps<{ id: string }>) {
  const [user, setUser] = useState<User | null>(null);
  const [error, setError] = useState<boolean>(false);
  const [loading, setLoading] = useState<boolean>(false);

  const id = props.match.params.id;

  useEffect(() => {
    setLoading(true);
    Axios.get<User>(`http://localhost:3001/users/${id}`)
      .then(res => setUser(res.data))
      .catch(() => setError(true))
      .finally(() => setLoading(false))
  }, [])

  return <div>
    { error && <ShowError /> }
    { loading && <div>Loading....</div>}
    <UserDetailsPanel user={user}/>
  </div>
}


// ===========
// UTILS
// ===========
function ShowError() {
  return <div className="alert alert-danger">
    Errore. Utente nn trovato!
  </div>
}

function UserDetailsPanel(props: { user: User | null}) {
  return props.user &&
          <Card title={props.user.name}>
            {props.user.id}
          </Card>
}
